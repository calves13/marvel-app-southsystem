package br.com.marvelapp.viewmodels.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.marvelapp.api.CharacterAPI
import br.com.marvelapp.api.CharacterApiService
import br.com.marvelapp.models.CharacterMarvel
import br.com.marvelapp.models.CharacterResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {

    val charactersResult = MutableLiveData<List<CharacterMarvel>>()
    val charactersError = MutableLiveData(false)

    /**
     * Funçao que chama a API para retornar a lista de personagens
     */
    fun getCharacters(){
        val characterService = CharacterApiService.getInstance().create(CharacterAPI::class.java)
        characterService.getCharacters().enqueue(object : Callback<CharacterResponse> {
            override fun onResponse(call: Call<CharacterResponse>, response: Response<CharacterResponse>) {
                resultCharactersSuccess(response.body()?.data?.results)
            }

            override fun onFailure(call: Call<CharacterResponse>, t: Throwable) {
                resultCharactersError()
            }
        })
    }

    /**
     * Funçao chamada quando a lista tem dados com sucesso.
     */
    fun resultCharactersSuccess(results: List<CharacterMarvel>?) {
        charactersResult.postValue(results ?: emptyList())
    }

    /**
     * Funçao chamada quando a API tem algum erro.
     */
    fun resultCharactersError(){
        charactersError.postValue(true)
    }
}