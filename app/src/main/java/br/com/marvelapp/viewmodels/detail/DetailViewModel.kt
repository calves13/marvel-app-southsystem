package br.com.marvelapp.viewmodels.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.marvelapp.api.CharacterAPI
import br.com.marvelapp.api.CharacterApiService
import br.com.marvelapp.models.CharacterMarvel
import br.com.marvelapp.models.CharacterResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailViewModel : ViewModel() {

    val characterResult = MutableLiveData<CharacterMarvel>()

    /**
     * Funçao que chama a API para buscar o personagem pelo seu ID.
     */
    fun getCharacterDetail(id: Long){
        val characterService = CharacterApiService.getInstance().create(CharacterAPI::class.java)
        characterService.getCharacterById(characterId = id.toString()).enqueue(object :
            Callback<CharacterResponse> {
            override fun onResponse(call: Call<CharacterResponse>, response: Response<CharacterResponse>) {
                response.body()?.data?.results?.first()?.let { resultCharactersSuccess(it) }
            }

            override fun onFailure(call: Call<CharacterResponse>, t: Throwable) {}
        })
    }

    /**
     * Funçao chamada quando as informaçoes fora retornadas com sucesso.
     */
    fun resultCharactersSuccess(result: CharacterMarvel) {
        characterResult.postValue(result)
    }

}