package br.com.marvelapp.models

data class CharacterResponse(
    val data: CharacterData
)

data class CharacterData(
    val results: List<CharacterMarvel>
)

data class CharacterMarvel(
    val id: Long,
    val name: String,
    val thumbnail: Thumbnail,
    val description: String,
    val series: Series
) {
    fun getImage(): String = "${thumbnail.path}.${thumbnail.extension}".replace("http", "https")
}

data class Thumbnail(
    val path: String,
    val extension: String
)

data class Series(
    val items: List<SeriesItem>
)

data class SeriesItem(
    val name: String
)