package br.com.marvelapp.ui.list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.marvelapp.R
import br.com.marvelapp.ui.detail.DetailActivity
import br.com.marvelapp.viewmodels.list.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val viewModel : MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prepareRecycleView()
        initObservers()

        viewModel.getCharacters()
    }

    /**
     * Funçao que Observa quando o ViewModel obter o resultado da API.
     */
    private fun initObservers(){
        viewModel.charactersResult.observe(this,{
            loading.visibility = View.GONE
            rv_character.adapter = CharacterAdapter(it ?: emptyList()) {
                openCharacterDetail(it)
            }
        })

        viewModel.charactersError.observe(this, {
            if (it == true){
                loading.visibility = View.GONE
                iv_bad.visibility = View.VISIBLE
                tv_error.visibility = View.VISIBLE
            }
        })
    }

    /**
     * Preparaçao do RecycleView de forma Linear.
     */
    private fun prepareRecycleView(){
        rv_character.layoutManager = LinearLayoutManager(this)
    }

    /**
     * Funçao que abre a tela (activity) de Detalhes dos personagens.
     */
    private fun openCharacterDetail(id: Long){
        val intentDetail = Intent(this, DetailActivity::class.java)
        intentDetail.putExtra("CHARACTER_ID",id)
        startActivity(intentDetail)
    }
}