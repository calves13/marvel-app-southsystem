package br.com.marvelapp.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.marvelapp.R
import br.com.marvelapp.models.CharacterMarvel
import br.com.marvelapp.models.SeriesItem
import coil.api.load
import kotlinx.android.synthetic.main.item_characters.view.*
import kotlinx.android.synthetic.main.item_serie.view.*

class SeriesAdapter (
    private val series: List<SeriesItem>
) : RecyclerView.Adapter<SeriesAdapter.SeriesViewHolder>(){

    inner class SeriesViewHolder(view: View) : RecyclerView.ViewHolder(view){

        fun bindCharacter(serie: SeriesItem){
            itemView.tv_series_value.text = serie.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeriesViewHolder {
        return SeriesViewHolder (
            LayoutInflater.from(parent.context).inflate(R.layout.item_serie, parent, false)
        )
    }

    override fun onBindViewHolder(holder: SeriesViewHolder, position: Int) {
        holder.bindCharacter(series[position])
    }

    override fun getItemCount(): Int = series.size
}
