package br.com.marvelapp.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.marvelapp.R
import br.com.marvelapp.models.CharacterMarvel
import coil.api.load
import kotlinx.android.synthetic.main.item_characters.view.*

class CharacterAdapter (
    private val character : List<CharacterMarvel>,
    private val characterId: (Long) -> Unit

) : RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>(){

    inner class CharacterViewHolder(view: View) : RecyclerView.ViewHolder(view){

        fun bindCharacter(character: CharacterMarvel){
            itemView.tv_name_character.text = character.name
            itemView.iv_character.load(character.getImage()) {
                crossfade(true)
                crossfade(300)
            }

            itemView.setOnClickListener {
                characterId.invoke(character.id)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        return CharacterViewHolder (
            LayoutInflater.from(parent.context).inflate(R.layout.item_characters, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.bindCharacter(character[position])
    }

    override fun getItemCount(): Int = character.size
}
