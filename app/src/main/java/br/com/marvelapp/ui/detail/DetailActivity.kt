package br.com.marvelapp.ui.detail

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.marvelapp.R
import br.com.marvelapp.models.CharacterMarvel
import br.com.marvelapp.viewmodels.detail.DetailViewModel
import coil.api.load
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    private val viewModel : DetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        prepareRecycleView()
        initObservers()

        val characterId = intent.extras?.getLong("CHARACTER_ID") ?: 0

        viewModel.getCharacterDetail(characterId)

        iv_back.setOnClickListener {
            finish()
        }
    }

    private fun initObservers(){
        viewModel.characterResult.observe(this,{
            characterInfo(it)
        })
    }

    /**
     * Funçao que carrega todos os dados do personagem na tela.
     */

    private fun characterInfo(character: CharacterMarvel?) {
        character?.let {
            if (character.description.isNullOrEmpty()){
                tv_description.visibility = View.GONE
            }
            if (character.series.items.isNullOrEmpty()){
                tv_series.visibility = View.GONE
            }

            iv_character.load(character.getImage())
            tv_name_character.text = character.name
            tv_description_value.text = character.description
            rv_series.adapter = SeriesAdapter(character.series.items)
        }
    }

    private fun prepareRecycleView(){
        rv_series.layoutManager = LinearLayoutManager(this)
    }
}