package br.com.marvelapp.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CharacterApiService {

    companion object{
        private const val BASE_URL = "https://gateway.marvel.com/v1/public/"
        private var retrofit: Retrofit? = null

        fun getInstance() :Retrofit{
            if(retrofit == null){
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit!!
        }
    }
}