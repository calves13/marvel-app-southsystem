package br.com.marvelapp.api

import br.com.marvelapp.models.CharacterResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CharacterAPI {

    @GET("/v1/public/characters")
    fun getCharacters(
        @Query("ts") ts: String = "1",
        @Query("apikey") apikey: String = "db1a1a64cd3f03a196e0eab46acb4290",
        @Query("hash") hash: String = "26c93fa53474af905bbaf730ba8bbdb2"
    ): Call<CharacterResponse>

    @GET("/v1/public/characters/{characterId}")
    fun getCharacterById(
        @Path("characterId") characterId: String,
        @Query("ts") ts: String = "1",
        @Query("apikey") apikey: String = "db1a1a64cd3f03a196e0eab46acb4290",
        @Query("hash") hash: String = "26c93fa53474af905bbaf730ba8bbdb2"
    ): Call<CharacterResponse>
}

