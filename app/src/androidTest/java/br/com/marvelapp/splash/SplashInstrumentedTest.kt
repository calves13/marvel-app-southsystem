package br.com.marvelapp.splash

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import br.com.marvelapp.R
import br.com.marvelapp.ui.splash.SplashActivity
import org.junit.Before
import org.junit.Test


@LargeTest
class SplashInstrumentedTest {

    @Before
    fun openSplash(){
        ActivityScenario.launch(SplashActivity::class.java)
    }

    @Test
    fun testLogoVisible(){
        onView(withId(R.id.iv_logo)).check(matches(isDisplayed()))
    }
}