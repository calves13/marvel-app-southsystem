Marvel App - Vaga de Android para Southsystem
---

## Arquiteura e Bibliotecas

Aqui você encontrará todas as bibliotecas utilizadas no projeto.

1. Refrofit - Permite usar API externas no App.
2. Coil - Nova biblioteca desenvolvida em Kotlin para carregamento de Images.
3. MVVM - Seguindo o padrao de projeto que o Google recomenda hoje em dia.
4. Testes Instrumentados (UI)  utilizando a biblioteca Junit + Espresso
5. Tratamento de Erros (falha conectivadade com internet e erro da API)
6. ProgressBar - Para o adicionar o LOADING enquando carrega a API.

---

## Telas de Exemplo do Marvel App em funcionamento

Tela de Splash
![Scheme](images/splash.jpg)

Tela de Lista de Personagens
![Scheme](images/lista.jpg)

Tela de Detalhe do Personagem
![Scheme](images/detalhe.jpg)

